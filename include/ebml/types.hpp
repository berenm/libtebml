/**
 * @file
 * @date 15 sept. 2010
 * @todo comment
 */

#ifndef EBML_TYPES_HPP_
#define EBML_TYPES_HPP_

#include <vector>
#include <boost/integer.hpp>

#define BOOST_DATE_TIME_POSIX_TIME_STD_CONFIG
#include <boost/date_time.hpp>

namespace ebml {

  typedef ::std::vector< ::boost::uint8_t > uint8_vector_t;

  typedef ::boost::uint64_t id_t;
  typedef ::boost::uint64_t size_t;

  // Values that cannot be used as real sizes for specific behavior.
  const ::ebml::size_t undefined_size = 0xFFFFFFFFFFFFFFFFUL;
  const ::ebml::size_t auto_size = 0x7FFFFFFFFFFFFFFFUL;

  typedef ::boost::uint64_t unsigned_integer_t;
  typedef ::boost::int64_t integer_t;
  typedef float float_t;
  typedef double double_t;
  typedef ::std::wstring string_t;
  typedef ::boost::posix_time::ptime datetime_t;

} // namespace ebml


#endif /* EBML_TYPES_HPP_ */
