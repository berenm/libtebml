/**
 * @file
 * @date 15 sept. 2010
 * @todo comment
 */

#ifndef EBML_ELEMENT_HPP_
#define EBML_ELEMENT_HPP_

#include "ebml/types.hpp"
#include "ebml/conversion.hpp"

namespace ebml {

  template< const ::ebml::id_t id_t, const ::ebml::size_t size_t = ::ebml::auto_size >
  struct element {
  };

} // namespace ebml

#endif /* EBML_ELEMENT_HPP_ */
