/**
 * @file
 * @date 15 sept. 2010
 * @todo comment
 */

#ifndef EBML_VALUE_ELEMENT_HPP_
#define EBML_VALUE_ELEMENT_HPP_

#include "ebml/types.hpp"
#include "ebml/conversion.hpp"

namespace ebml {

  template< typename value_type_t >
  struct value_element {
      typedef value_type_t value_t;

      value_element(const ::ebml::id_t& id_in, const ::ebml::size_t& size_in = 0) :
        id(id_in), bytes(::ebml::uint8_vector_t(size_in, 0)), value(value_t()) {
      }
      value_element(const ::ebml::value_element< value_t >& copy_in) :
        id(copy_in.id), bytes(copy_in.bytes), value(copy_in.value) {
      }
      ::ebml::value_element< value_t >& operator=(const ::ebml::value_element< value_t >& copy_in) {
        id = copy_in.id;
        bytes = copy_in.bytes;
        value = copy_in.value;
        return *this;
      }

      ::ebml::id_t get_id() const {
        return id;
      }

      ::ebml::size_t get_size() const {
        return bytes.size();
      }

      value_t get_value() const {
        return value;
      }

      const ::boost::uint8_t* get_bytes() const {
        return bytes.data();
      }

      void set_value(const value_t& value_in) {
        value = value_in;
        bytes = ::ebml::conversion< value_t >::to_bytes(value);
      }

      void set_bytes(const ::ebml::uint8_vector_t& bytes_in) {
        bytes = bytes_in;
        value = ::ebml::conversion< value_t >::from_bytes(bytes);
      }

    protected:
      ::ebml::id_t id;
      ::ebml::uint8_vector_t bytes;

      value_t value;
  };

  template< >
  struct value_element< ::ebml::uint8_vector_t > {
      typedef ::ebml::uint8_vector_t value_t;

      value_element(const ::ebml::id_t& id_in, const ::ebml::size_t& size_in = 0) :
        id(id_in), bytes(::ebml::uint8_vector_t(size_in, 0)) {
      }
      value_element(const ::ebml::value_element< value_t >& copy_in) :
        id(copy_in.id), bytes(copy_in.bytes) {
      }
      ::ebml::value_element< value_t >& operator=(const ::ebml::value_element< value_t >& copy_in) {
        id = copy_in.id;
        bytes = copy_in.bytes;
        return *this;
      }

      ::ebml::id_t get_id() const {
        return id;
      }

      ::ebml::size_t get_size() const {
        return bytes.size();
      }

      value_t get_value() const {
        return bytes;
      }

      const ::boost::uint8_t* get_bytes() const {
        return bytes.data();
      }

      void set_value(const value_t& value_in) {
        bytes = value_in;
      }

      void set_bytes(const ::ebml::uint8_vector_t& bytes_in) {
        bytes = bytes_in;
      }

    protected:
      ::ebml::id_t id;
      ::ebml::uint8_vector_t bytes;
  };

} // namespace ebml

#endif /* EBML_VALUE_ELEMENT_HPP_ */
