/**
 * @file
 * @date 15 sept. 2010
 * @todo comment
 */

#ifndef EBML_CONVERSION_HPP_
#define EBML_CONVERSION_HPP_

#include "ebml/types.hpp"

namespace ebml {

  template< typename value_type_t >
  struct conversion {
      static uint8_vector_t to_bytes(const value_type_t& value_in);
      static value_type_t from_bytes(const uint8_vector_t& bytes_in);
  };

} // namespace ebml


#endif /* EBML_CONVERSION_HPP_ */
