%filenames parser
%scanner ../scanner/scanner.h

                                // lowest precedence
%token  t_declare
        t_define
        t_header
        t_types
        t_elements
        t_children
        t_container
        t_int
        t_uint
        t_float
        t_string
        t_date
        t_binary
        t_cardinality
        t_default
        t_level
        t_ordered
        t_parent
        t_range
        t_size
        t_yes
        t_no
        t_version
        t_read_version
        t_max_id_length
        t_max_size_length
        t_doctype
        t_doctype_version
        t_doctype_read_version
        t_colon
        t_semic
        t_comma
        t_to
        t_inte
        t_star
        t_plus
        t_one
        t_lower
        t_lowereq
        t_greater
        t_greatereq
        t_lparen
        t_rparen
        t_lbrack
        t_rbrack
        t_lbrace
        t_rbrace
        t_is
        t_date_cst
        t_hex_cst
        t_string_cst
        t_nint_cst
        t_pint_cst
        t_decimal_cst
        t_exponent_cst
        t_id
        t_name
        t_alpha

%start dtd

%%

// ======================================================================================================
// Default property values
// ======================================================================================================
v_default_int: t_pint_cst | t_nint_cst ;
v_default_uint: t_pint_cst ;
v_default_float: t_decimal_cst { | t_exponent_cst } ;
v_default_date: v_default_int | t_date_cst ;
v_string_hex: t_hex_cst ;
v_string_lit: t_string_cst ;
v_default_string: { v_string_hex | v_string_lit } ;
v_default_binary: v_default_string ;

// ======================================================================================================
// Parent property values
// ======================================================================================================
v_parent: { t_name | t_name t_comma v_parent } ;

// ======================================================================================================
// Level property values
// ======================================================================================================
v_level: t_pint_cst { | t_to t_pint_cst } ;

// ======================================================================================================
// Cardinality property values
// ======================================================================================================
v_card: { t_inte | t_star | t_one | t_plus } ;

// ======================================================================================================
// Ordered property values
// ======================================================================================================
v_ordered: { t_yes | t_one | t_no | t_zero } ;

// ======================================================================================================
// Range property values
// ======================================================================================================
v_range_int_item: { v_default_int
                  | v_default_int t_to
                  | t_to v_default_int
                  | v_default_int t_to v_default_int } ;
v_range_int: { v_range_int_item | v_range_int_item t_colonmma v_range_int } ;

v_range_uint_item: v_default_uint { | t_to v_default_uint } ;
v_range_uint: { v_range_uint_item | v_range_uint_item t_comma v_range_uint } ;

v_range_float_item: { t_lower v_default_float
                    | t_lowereq v_default_float
                    | t_greater v_default_float
                    | t_greatereq v_default_float
                    | v_default_float { t_lower | t_lowereq } t_to { t_lower | t_lowereq } v_default_float } ;
v_range_float: { v_range_float_item | v_range_float_item t_comma v_range_float } ;

v_range_date_item: { v_default_date t_to
                   | t_to v_default_date
                   | v_default_date t_to v_default_date } ;
v_range_date: { v_range_date_item | v_range_date_item t_comma v_range_date } ;

v_range_string_item: v_range_uint_item ;
v_range_string: { v_range_string_item | v_range_string_item t_comma v_range_string } ;

v_range_binary_item: v_range_uint_item ;
v_range_binary: { v_range_binary_item | v_range_binary_item t_comma v_range_binary } ;

// ======================================================================================================
// Size property values
// ======================================================================================================
v_size: { v_range_uint | v_range_uint t_comma v_size } ;

p_card    : t_cardinality t_colon v_card    t_semic ;
p_level   : t_level       t_colon v_level   t_semic ;
p_ordered : t_ordered     t_colon v_ordered t_semic ;
p_parent  : t_parent      t_colon v_parent  t_semic ;
p_size    : t_size        t_colon v_size    t_semic ;

p_default_int    : t_default t_colon v_default_int    t_semic ;
p_default_uint   : t_default t_colon v_default_uint   t_semic ;
p_default_float  : t_default t_colon v_default_float  t_semic ;
p_default_date   : t_default t_colon v_default_date   t_semic ;
p_default_string : t_default t_colon v_default_string t_semic ;
p_default_binary : t_default t_colon v_default_binary t_semic ;

p_range_int    : t_range t_colon v_range_int    t_semic ;
p_range_uint   : t_range t_colon v_range_uint   t_semic ;
p_range_float  : t_range t_colon v_range_float  t_semic ;
p_range_date   : t_range t_colon v_range_date   t_semic ;
p_range_string : t_range t_colon v_range_string t_semic ;
p_range_binary : t_range t_colon v_range_binary t_semic ;

p_property_int    : { p_card | p_level | p_ordered | p_parent | p_size | p_default_int    | p_range_int    } ;
p_property_uint   : { p_card | p_level | p_ordered | p_parent | p_size | p_default_uint   | p_range_uint   } ;
p_property_float  : { p_card | p_level | p_ordered | p_parent | p_size | p_default_float  | p_range_float  } ;
p_property_date   : { p_card | p_level | p_ordered | p_parent | p_size | p_default_date   | p_range_date   } ;
p_property_string : { p_card | p_level | p_ordered | p_parent | p_size | p_default_string | p_range_string } ;
p_property_binary : { p_card | p_level | p_ordered | p_parent | p_size | p_default_binary | p_range_binary } ;
p_property        : { p_card | p_level | p_ordered | p_parent | p_size } ;

p_property_ints     : { p_property_int    | p_property_int    p_property_ints     } ;
p_property_uints    : { p_property_uint   | p_property_uint   p_property_uints    } ;
p_property_floats   : { p_property_float  | p_property_float  p_property_floats   } ;
p_property_dates    : { p_property_date   | p_property_date   p_property_dates    } ;
p_property_strings  : { p_property_string | p_property_string p_property_strings  } ;
p_property_binaries : { p_property_binary | p_property_binary p_property_binaries } ;
p_properties        : { p_property        | p_property        p_properties        } ;

properties_int    : t_lbrack p_property_ints     t_rbrack ;
properties_uint   : t_lbrack p_property_uints    t_rbrack ;
properties_float  : t_lbrack p_property_floats   t_rbrack ;
properties_date   : t_lbrack p_property_dates    t_rbrack ;
properties_string : t_lbrack p_property_strings  t_rbrack ;
properties_binary : t_lbrack p_property_binaries t_rbrack ;
properties        : t_lbrack p_properties        t_rbrack ;

// ======================================================================================================
// Types
// ======================================================================================================
type_contnr : { t_container | v_name } ;
type_int    : { t_int       | v_name } ;
type_uint   : { t_uint      | v_name } ;
type_float  : { t_float     | v_name } ;
type_date   : { t_date      | v_name } ;
type_string : { t_string    | v_name } ;
type_binary : { t_binary    | v_name } ;
type_value  : { type_int | type_uint | type_float | type_date | type_string | type_binary } ;
type_any    : { type_value | type_contnr } ;

t_dect_contnr : v_name t_is t_contnr { properties        { | t_semic } | t_semic } ;
t_decl_int    : v_name t_is t_int    { properties_int    { | t_semic } | t_semic } ;
t_decl_uint   : v_name t_is t_uint   { properties_uint   { | t_semic } | t_semic } ;
t_decl_float  : v_name t_is t_float  { properties_float  { | t_semic } | t_semic } ;
t_decl_date   : v_name t_is t_date   { properties_date   { | t_semic } | t_semic } ;
t_decl_string : v_name t_is t_string { properties_string { | t_semic } | t_semic } ;
t_decl_binary : v_name t_is t_binary { properties_binary { | t_semic } | t_semic } ;
t_decl_value  : { t_decl_int | t_decl_uint | t_decl_float | t_decl_date | t_decl_string | t_decl_binary } ;
t_decl_any    : { t_dect_colonntnr | t_decl_value } ;
t_decl_anys : { | t_decl_any t_decl_anys } ;

t_block : t_define t_types t_lbrace t_decl_anys t_rbrace ;

// ======================================================================================================
// Elements
// ======================================================================================================
e_contnr : v_name t_is v_id t_contnr { | properties }  { t_lbrace e_anys t_rbrace | t_semic } ;
e_int    : v_name t_is v_id t_int    { properties_int    { | t_semic } | t_semic } ;
e_uint   : v_name t_is v_id t_uint   { properties_uint   { | t_semic } | t_semic } ;
e_float  : v_name t_is v_id t_float  { properties_float  { | t_semic } | t_semic } ;
e_date   : v_name t_is v_id t_date   { properties_date   { | t_semic } | t_semic } ;
e_string : v_name t_is v_id t_string { properties_string { | t_semic } | t_semic } ;
e_binary : v_name t_is v_id t_binary { properties_binary { | t_semic } | t_semic } ;
e_value  : { e_int | e_uint | e_float | e_date | e_string | e_binary } ;
e_any    : { e_value | e_contnr | t_children } ;
e_anys : { | e_any e_anys } ;

e_block : t_define t_elements t_lbrace e_anys t_rbrace ;

// ======================================================================================================
// Header
// ======================================================================================================
h_version              : t_version              t_is v_default_uint   t_semic ;
h_read_version         : t_read_version         t_is v_default_uint   t_semic ;
h_max_id_length        : t_max_id_length        t_is v_default_uint   t_semic ;
h_max_size_length      : t_max_size_length      t_is v_default_uint   t_semic ;
h_doctype              : t_doctype              t_is v_default_string t_semic ;
h_doctype_version      : t_doctype_version      t_is v_default_uint   t_semic ;
h_doctype_read_version : t_doctype_read_version t_is v_default_uint   t_semic ;
h_statement : { h_version | h_read_version | h_max_id_length | h_max_size_length | h_doctype | h_doctype_version | h_doctype_read_version } ;
h_statements : { | h_statement h_statements } ;

h_block : t_declare t_header t_lbrace h_statements t_rbrace ;

// ======================================================================================================
// DTD
// ======================================================================================================
dtd : { | { h_block | t_block | e_block } dtd } ;

%%











// ======================================================================================================
// Structures
// ======================================================================================================

// NOTE: This BNF is not correct in that it allows for more freedom
// than what is described in the text. That is because a 100%
// correct BNF would be almost unreadable. To be correct CELEMENT
// would be split into one ELEMENT token for every value type, and
// then each and every one of them would have their own PROPERTIES
// definition which points out only the DEF and RANGE for that value
// type. Some other shortcuts are noted in comments.

DTD: { H_BLOCK | T_BLOCK | E_BLOCK } DTD ;

H_BLOCK: t_declare t_header '{' H_STATEMENTS '}' ;
E_BLOCK: t_define t_elements '{' e_any '}' ;

STATEMENTS: | STATEMENT STATEMENTS ;
DELEMENTS: | DELEMENT DELEMENTS ;

/*
EBLOCK   : "define" S WSP "elements" S "{" *(S / DELEMENT) "}"
TBLOCK   : "define" S WSP "types" S "{" *(S / DTYPE) "}"

DELEMENT : VELEMENT / CELEMENT / "%children;"
VELEMENT : NAME S ":=" S ID WSP S TYPE S (PROPERTIES S *1";")/";"
CELEMENT : NAME S ":=" S ID WSP S "container" S *1PROPERTIES S
        ("{" *DELEMENT "}")/";"

NAME     : [A-Za-z_] 1*[A-Za-z_0-9]

ID       : 1*( 2HEXDIG )

TYPE     : VTYPE / CTYPE
VTYPE    : "int" / "uint" / "float" / "string" / "date" /
        "binary" / NAME
CTYPE    : "conainer" / NAME

PROPERTIES : "[" S 1*PROPERTY S "]"
PROPERTY   : PARENT / LEVEL / CARD / DEF / RANGE / SIZE

PARENT   : "parent" S ":" S PARENTS S ";"
PARENTS  : NAME / ( NAME S "," S PARENTS )

LEVEL    : "level"  S ":" S 1*DIGIT *(".." *DIGIT) S ";"
CARD     : "card"   S ":" S ( "*" / "?" / "1" / "+" ) S ";"

ORDERED  : "ordered" S ":" S ( YES / NO ) S ";"
YES      : "yes" / "1"
NO       : "no" / "0"

DEF      : "def"    S ":" S DEFS S ";"
DEFS     : ( INT_DEF / UINT_DEF / FLOAT_DEF / STRING_DEF /
          DATE_DEF / BINARY_DEF / NAME )

RANGE    : "range" S ":" S RANGE_LIST S ";"
RANGE_LIST : RANGE_ITEM / ( RANGE_ITEM S "," S RANGE_LIST )
RANGE_ITEM : INT_RANGE / UINT_RANGE / FLOAT_RANGE /
          STRING_RANGE / DATE_RANGE / BINARY_RANGE

SIZE       : "size" S ":" S SIZE_LIST S ";"
SIZE_LIST  : UINT_RANGE / ( UINT_RANGE S "," S SIZE_LIST )

; Actual values, but INT_VALUE is too long.
INT_V   : *1"-" 1*DIGIT
FLOAT_V : INT "." 1*DIGIT *1( "e" *1( "+"/"-" ) 1*DIGIT )
; DATE uses ISO short format, yyyymmddThh:mm:ss.f
DATE_V  : *1DIGIT 2DIGIT 2DIGIT
       *1(%x54 2DIGIT ":" 2DIGIT ":" 2DIGIT *1( "." *1DIGIT ))

INT_DEF    : INT_V
UINT_DEF   : 1*DIGIT
FLOAT_DEF  : FLOAT_V
DATE_DEF   : INT_DEF / DATE_V
STRING_DEF : ("0x" 1*( 2HEXDIG )) / ( %x22 *(%x20-7e) %x22 )
BINARY_DEF : STRING_DEF

INT_RANGE   : INT_V / ( INT_V ".." ) / ( ".." INT_V ) /
            ( INT_V ".." INT_V )
UINT_RANGE  : 1*DIGIT *1( ".." *DIGIT )
FLOAT_RANGE : ( ("<" / "<=" / ">" / ">=") FLOAT_DEF ) /
             ( FLOAT_DEF "<"/"<=" ".." "<"/"<=" FLOAT_DEF )
DATE_RANGE   : (1*DIGIT / DATE_V) *1( ".." *(DIGIT / DATE_V) )
BINARY_RANGE : UINT_RANGE
STRING_RANGE : UINT_RANGE

STATEMENT : NAME S ":=" S DEFS S ";"

; TYPE must be defined. PROPERTIES must only use DEF and RANGE.
DTYPE     : NAME S ":=" S TYPE S (PROPERTIES S *1";")/";"
*/
