/**
 * @file
 * @date 13 sept. 2010
 * @todo comment
 */

#include "ebml/conversion.hpp"
#include "ebml/value_element.hpp"

#include <iostream>
#include <sstream>

namespace ebml {

  template< const ::ebml::id_t parent_id_t, const ::ebml::id_t child_id_t >
  struct can_be_child_of: ::boost::false_type {
  };

  template< const ::ebml::id_t id_t, const ::std::size_t level_t >
  struct can_be_at_level: ::boost::false_type {
  };

  struct ebml_listener {
//      void on_start_tag(const element_type1& element);
//      void on_end_tag(const element_type1& element);
//      void on_value(const value_element_type1& element);
  };

  typedef ::std::vector< ::boost::uint8_t > uint8_vector;

  struct container {
      container(const ::boost::uint64_t& id_in, const ::boost::uint64_t& size_in = 0) :
        id(id_in), size(size_in) {
      }
      container(const container& copy_in) :
        id(copy_in.id), size(copy_in.size) {
      }
      container& operator=(const container& copy_in) {
        id = copy_in.id;
        size = copy_in.size;
        return *this;
      }

      ::boost::uint64_t get_id() const {
        return id;
      }

      ::boost::uint64_t get_size() const {
        return size;
      }

    protected:
      ::boost::uint64_t id;
      ::boost::uint64_t size;
  };

  struct istream: public ::std::basic_istringstream< ::boost::uint8_t > {
      istream(const ::std::basic_string< ::boost::uint8_t >& str) :
        ::std::basic_istringstream< ::boost::uint8_t >(str) {
      }

      template< typename value_type_t >
      ::ebml::value_element< value_type_t > read_element() {
        ::boost::uint64_t id = read_id();
        ::boost::uint64_t size = read_size();
        uint8_vector bytes(size, 0);
        read(bytes.data(), bytes.size());

        ::ebml::value_element< value_type_t > element_out(id, size);
        element_out.set_bytes(bytes);

        return element_out;
      }

      ::boost::uint64_t read_id() {
        return read_vuint();
      }

      ::boost::uint64_t read_size() {
        return read_vuint();
      }

      ::boost::uint64_t read_vuint() {
        ::std::size_t vuint_size = 0;
        ::std::size_t bytes_read = 0;
        ::boost::uint8_t mask = 0b00000000;
        ::boost::uint8_t byte = 0b00000000;

        // Read bytes until the mask matches the current byte.
        // Increment size at each mask shift.
        while (mask == 0 || (byte & mask) != mask) {
          // At each iteration, shift the mask to the right.
          mask >>= 1;

          // If mask is 0, we are at the begining or we have made a loop in the mask, meaning we need to read another byte.
          if (mask == 0) {
            // If we made a loop, the previous byte is only padding of 0's for the size marker, so we don't care.

            // Read a new byte.
            read(&byte, 1);
            // Reset the mask.
            mask = 0b10000000;
            // Increment the number of bytes we already have read.
            ++bytes_read;
          }

          // And increment the size by one.
          ++vuint_size;
        }

        // Remove the size indicator from the current byte.
        byte ^= mask;
        // We only have to read (size - read) bytes more.
        vuint_size -= bytes_read;

        // Actually, the current byte might be significant, if it's not full of 0's, we will remove it later if necessary.
        uint8_vector bytes = uint8_vector(vuint_size + 1, byte);
        this->read(bytes.data() + 1, vuint_size);

        // If the first byte was full of 0's, just remove it, the size marker was at the end of a byte.
        if (byte == 0) {
          bytes.erase(bytes.begin());
        }

        return ::ebml::conversion< ::ebml::unsigned_integer_t >::from_bytes(bytes);
      }
  };

  struct ostream: public ::std::basic_ostringstream< ::boost::uint8_t > {
      template< typename value_type_t >
      void write_element(const ::ebml::value_element< value_type_t >& element_in) {
        write_id(element_in.get_id());
        write_size(element_in.get_size());
        write(element_in.get_bytes(), element_in.get_size());
      }

      void write_id(const ::boost::uint64_t& id) {
        write_vuint_restricted(id);
      }

      void write_size(const ::boost::uint64_t& size) {
        write_vuint_restricted(size);
      }

      void write_vuint_restricted(const ::boost::uint64_t& value) {
        typedef ::ebml::conversion< ::ebml::unsigned_integer_t > conversion_t;
        uint8_vector bytes;

        switch (value) {
          case 0x7F:
            bytes = conversion_t::to_bytes(0x407FUL);
            this->write(bytes.data(), bytes.size());
            break;
          case 0x3FFF:
            bytes = conversion_t::to_bytes(0x203FFFUL);
            this->write(bytes.data(), bytes.size());
            break;
          case 0x1FFFFF:
            bytes = conversion_t::to_bytes(0x101FFFFFUL);
            this->write(bytes.data(), bytes.size());
            break;
          case 0xFFFFFFFL:
            bytes = conversion_t::to_bytes(0x080FFFFFFFUL);
            this->write(bytes.data(), bytes.size());
            break;
          case 0x7FFFFFFFFL:
            bytes = conversion_t::to_bytes(0x0407FFFFFFFFUL);
            this->write(bytes.data(), bytes.size());
            break;
          case 0x3FFFFFFFFFFFL:
            bytes = conversion_t::to_bytes(0x023FFFFFFFFFFFUL);
            this->write(bytes.data(), bytes.size());
            break;
          case 0x1FFFFFFFFFFFFFL:
            bytes = conversion_t::to_bytes(0x011FFFFFFFFFFFFFUL);
            this->write(bytes.data(), bytes.size());
            break;
          default:
            write_vuint(value);
        }
      }

      void write_vuint(const ::boost::uint64_t& integer_in) {
        uint8_vector bytes = ::ebml::conversion< ::ebml::unsigned_integer_t >::to_bytes(integer_in);

        ::std::size_t minimal_size = bytes.size();
        if (minimal_size > 0) {
          ::boost::uint8_t first = bytes[0];
          ::std::size_t size = 8;

          for (::std::size_t i = 0; i < 8; ++i) {
            ::boost::uint8_t mask = 0xFFFF << i;
            if ((first & mask) == 0) {
              size = i;
              break;
            }
          }

          if (size + minimal_size - 1 < 8) {
            --minimal_size;
            bytes[0] |= (0b10000000 >> minimal_size);
          } else {
            bytes.insert(bytes.begin(), (0b10000000 >> minimal_size));
          }

          this->write(bytes.data(), bytes.size());
        } else {
          ::boost::uint8_t marker = 0b10000000;
          this->write(&marker, 1);
        }
      }
  };
}

int main(int argc, char **argv) {
  ::std::locale locale("");
  ::std::locale::global(locale);

  ::ebml::value_element< ::std::wstring > e(1);

  ebml::uint8_vector bytes;
  bytes.push_back(0x45);
  bytes.push_back(0x00);
  bytes.push_back(0x00);
  bytes.push_back(0x00);

  e.set_bytes(bytes);

  ebml::ostream out;
  out.write_element(e);

  ebml::istream in(out.str());
  ::ebml::value_element< ::std::wstring > result = in.read_element< ::std::wstring > ();

  ::std::wcout << result.get_value() << ::std::endl;
}

