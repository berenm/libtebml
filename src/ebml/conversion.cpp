/**
 * @file
 * @date 15 sept. 2010
 * @todo comment
 */

#include "ebml/conversion.hpp"

namespace ebml {

  template< >
  ::ebml::datetime_t conversion< ::ebml::datetime_t >::from_bytes(const ::ebml::uint8_vector_t& bytes_in) {
    ::ebml::datetime_t origin(::boost::gregorian::date(2001, ::boost::gregorian::Jan, 01),
                              ::boost::posix_time::nanoseconds(0));
    ::ebml::datetime_t datetime_out;

    if (bytes_in.size() != 8) {
      // If we do not have 8 bytes, it's probably not a datetime
      ::std::cerr << "Error, datetime values must be 8 bytes long, got " << bytes_in.size() << " bytes." << ::std::endl;
    } else {
      const ::boost::int64_t nanoseconds = *reinterpret_cast< const ::boost::int64_t* > (bytes_in.data());
      datetime_out = origin + ::boost::posix_time::nanoseconds(nanoseconds);
    }

    return datetime_out;
  }

  template< >
  ::ebml::uint8_vector_t conversion< ::ebml::datetime_t >::to_bytes(const ::ebml::datetime_t& datetime_in) {
    ::ebml::datetime_t origin(::boost::gregorian::date(2001, ::boost::gregorian::Jan, 01),
                              ::boost::posix_time::nanoseconds(0));
    ::boost::posix_time::time_duration duration = datetime_in - origin;
    ::boost::uint64_t nanoseconds = duration.total_nanoseconds();

    ::ebml::uint8_vector_t bytes_out(8, 0);

    const ::boost::uint8_t* bytes = reinterpret_cast< const ::boost::uint8_t* > (&nanoseconds);
    bytes_out.assign(bytes, bytes + 8);

    return bytes_out;
  }

  template< >
  ::ebml::double_t conversion< ::ebml::double_t >::from_bytes(const ::ebml::uint8_vector_t& bytes_in) {
    ::ebml::double_t double_out = 0;

    if (bytes_in.size() == 0) {
      return double_out;
    } else if (bytes_in.size() != 8) {
      // If we do not have 8 bytes, it's probably not a double
      ::std::cerr << "Error, double values must be 8 bytes long, got " << bytes_in.size() << " bytes." << ::std::endl;
    } else {
      double_out = *reinterpret_cast< const ::ebml::double_t* > (bytes_in.data());
    }

    return double_out;
  }

  template< >
  ::ebml::uint8_vector_t conversion< ::ebml::double_t >::to_bytes(const ::ebml::double_t& double_in) {
    ::ebml::uint8_vector_t bytes_out(8, 0);

    const ::boost::uint8_t* bytes = reinterpret_cast< const ::boost::uint8_t* > (&double_in);
    bytes_out.assign(bytes, bytes + 8);

    return bytes_out;
  }

  template< >
  ::ebml::float_t conversion< ::ebml::float_t >::from_bytes(const ::ebml::uint8_vector_t& bytes_in) {
    ::ebml::float_t float_out = 0;

    if (bytes_in.size() == 0) {
      return float_out;
    } else if (bytes_in.size() != 4) {
      // If we do not have 4 bytes, it's probably not a float
      ::std::cerr << "Error, float values must be 4 bytes long, got " << bytes_in.size() << " bytes." << ::std::endl;
    } else {
      float_out = *reinterpret_cast< const ::ebml::float_t* > (bytes_in.data());
    }

    return float_out;
  }

  template< >
  ::ebml::uint8_vector_t conversion< ::ebml::float_t >::to_bytes(const ::ebml::float_t& float_in) {
    ::ebml::uint8_vector_t bytes_out(4, 0);

    const ::boost::uint8_t* bytes = reinterpret_cast< const ::boost::uint8_t* > (&float_in);
    bytes_out.assign(bytes, bytes + 4);

    return bytes_out;
  }

  template< >
  ::ebml::string_t conversion< ::ebml::string_t >::from_bytes(const ::ebml::uint8_vector_t& bytes_in) {
    ::std::wstringstream stream;

    ::ebml::uint8_vector_t::const_iterator it;
    wchar_t character;
    bool first;
    bool error;
    ::boost::uint8_t byte;
    ::std::size_t size;

    for (it = bytes_in.begin(); it != bytes_in.end();) {
      byte = *it;
      if ((byte & 0b11110000) == 0b11110000) {
        size = 4;
      } else if ((byte & 0b11100000) == 0b11100000) {
        size = 3;
      } else if ((byte & 0b11000000) == 0b11000000) {
        size = 2;
      } else if ((~byte & 0b10000000) == 0b10000000) {
        size = 1;
      } else {
        ::std::cerr << "Invalid utf-8 sequence." << ::std::endl;
        return stream.str();
      }

      error = false;
      first = true;
      character = 0;
      do {
        switch (byte) {
          case 0xC0:
          case 0xC1:
          case 0xF5:
          case 0xF6:
          case 0xF7:
          case 0xF8:
          case 0xF9:
          case 0xFA:
          case 0xFB:
          case 0xFC:
          case 0xFD:
          case 0xFE:
          case 0xFF:
            ::std::cerr << "Invalid utf-8 sequence." << ::std::endl;
            error = true;
          default:
            if (!first && ((byte & 0b10000000) ^ (~byte & 0b01000000)) != 0b11000000) {
              // We've found a byte that is not one of first bytes and that do not starts with 0b10xxxxxx
              ::std::cerr << "Invalid utf-8 sequence." << ::std::endl;
              error = true;
            } else {
              character <<= 6;
              if (first && size > 1) {
                byte &= 0b01111111 >> size;
              }
              character |= (byte & 0b01111111);

              ++it;
              byte = *it;
              first = false;
            }
            break;
        }
      } while (!error && --size > 0);

      if (!error) {
        stream << character;
      } else {
        stream << 0xFFFD;
      }
    }

    return stream.str();
  }

  template< >
  ::ebml::uint8_vector_t conversion< ::ebml::string_t >::to_bytes(const ::ebml::string_t& string_in) {
    ::ebml::string_t::const_iterator it;
    ::ebml::uint8_vector_t bytes_out;
    ::boost::uint8_t byte;
    wchar_t character;

    for (it = string_in.begin(); it != string_in.end(); ++it) {
      character = *it;

      if (0 <= character && character <= 0x7F) {
        bytes_out.push_back(static_cast< ::boost::uint8_t > (character & 0b01111111));
      } else if (0x80 <= character && character <= 0x7FF) {
        byte = static_cast< ::boost::uint8_t > ((character >> 6) & 0b00011111);
        bytes_out.push_back(0b11000000 | byte);
        byte = static_cast< ::boost::uint8_t > (character & 0b00111111);
        bytes_out.push_back(0b10000000 | byte);
      } else if (0x800 <= character && character <= 0xFFFF) {
        byte = static_cast< ::boost::uint8_t > ((character >> 12) & 0b00001111);
        bytes_out.push_back(0b11100000 | byte);
        byte = static_cast< ::boost::uint8_t > ((character >> 6) & 0b00111111);
        bytes_out.push_back(0b10000000 | byte);
        byte = static_cast< ::boost::uint8_t > (character & 0b00111111);
        bytes_out.push_back(0b10000000 | byte);
      } else if (0x10000 <= character && character <= 0x10FFFF) {
        byte = static_cast< ::boost::uint8_t > ((character >> 18) & 0b00000111);
        bytes_out.push_back(0b11110000 | byte);
        byte = static_cast< ::boost::uint8_t > ((character >> 12) & 0b00111111);
        bytes_out.push_back(0b10000000 | byte);
        byte = static_cast< ::boost::uint8_t > ((character >> 6) & 0b00111111);
        bytes_out.push_back(0b10000000 | byte);
        byte = static_cast< ::boost::uint8_t > (character & 0b00111111);
        bytes_out.push_back(0b10000000 | byte);
      } else {
        ::std::wcerr << "Invalid unicode sequence, character " << character << " cannot be converted to utf-8."
            << ::std::endl;
      }
    }

    return bytes_out;
  }

  template< >
  ::ebml::unsigned_integer_t conversion< ::ebml::unsigned_integer_t >::from_bytes(const ::ebml::uint8_vector_t& bytes_in) {
    ::ebml::unsigned_integer_t integer_out = 0;

    if (bytes_in.size() > 8) {
      // If we got more than 8 bytes, we cannot even convert it to ::boost::uin64_t, this is too much.
      ::std::cerr << "Error, integer value too large, got " << bytes_in.size() << " bytes." << ::std::endl;
    } else {
      ::ebml::uint8_vector_t::const_iterator it;
      for (it = bytes_in.begin(); it != bytes_in.end(); ++it) {
        integer_out <<= 8;
        integer_out |= *it;
      }
    }

    return integer_out;
  }

  template< >
  ::ebml::uint8_vector_t conversion< ::ebml::unsigned_integer_t >::to_bytes(const ::ebml::unsigned_integer_t& integer_in) {
    ::ebml::uint8_vector_t bytes_out(8, 0);

    ::std::size_t minimal_size;
    ::boost::uint8_t byte;
    for (::std::size_t i = 0; i < 8; ++i) {
      byte = static_cast< ::boost::uint8_t > (0xFFFF & (integer_in >> (i * 8)));
      if (byte != 0) {
        minimal_size = i;
      }

      bytes_out[7 - i] = byte;
    }

    bytes_out.erase(bytes_out.begin(), bytes_out.begin() + 7 - minimal_size);

    return bytes_out;
  }

  template< >
  ::ebml::integer_t conversion< ::ebml::integer_t >::from_bytes(const ::ebml::uint8_vector_t& bytes_in) {
    return static_cast< ::ebml::integer_t > (conversion< ::ebml::unsigned_integer_t >::from_bytes(bytes_in));
  }

  template< >
  ::ebml::uint8_vector_t conversion< ::ebml::integer_t >::to_bytes(const ::ebml::integer_t& integer_in) {
    return conversion< ::ebml::unsigned_integer_t >::to_bytes(static_cast< ::ebml::unsigned_integer_t > (integer_in));
  }

} // namespace ebml
