/**
 * @file
 * @date 16 sept. 2010
 * @todo comment
 */

#include <boost/spirit/include/qi.hpp>
#include <boost/fusion/adapted/struct/adapt_struct.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/spirit/include/phoenix.hpp>

#include <fstream>

namespace qi = boost::spirit::qi;

namespace esd {
  namespace header {
    struct ebml_version {
        ::std::string name;
        ::boost::uint64_t value;
    };
    struct ebml_read_version {
        ::std::string name;
        ::boost::uint64_t value;
    };
    struct ebml_max_id_length {
        ::std::string name;
        ::boost::uint64_t value;
    };
    struct ebml_max_size_length {
        ::std::string name;
        ::boost::uint64_t value;
    };
    struct doctype {
        ::std::string name;
        ::std::string value;
    };
    struct doctype_version {
        ::std::string name;
        ::boost::uint64_t value;
    };
    struct doctype_read_version {
        ::std::string name;
        ::boost::uint64_t value;
    };
  } // namespace header

  typedef ::boost::variant< header::ebml_version, header::ebml_read_version, header::ebml_max_id_length,
      header::ebml_max_size_length, header::doctype, header::doctype_version, header::doctype_read_version > header_t;
} // namespace esd

BOOST_FUSION_ADAPT_STRUCT(
    ::esd::header::ebml_version,
    (::std::string, name)
    (::boost::uint64_t, value)
)
BOOST_FUSION_ADAPT_STRUCT(
    ::esd::header::ebml_read_version,
    (::std::string, name)
    (::boost::uint64_t, value)
)
BOOST_FUSION_ADAPT_STRUCT(
    ::esd::header::ebml_max_id_length,
    (::std::string, name)
    (::boost::uint64_t, value)
)
BOOST_FUSION_ADAPT_STRUCT(
    ::esd::header::ebml_max_size_length,
    (::std::string, name)
    (::boost::uint64_t, value)
)
BOOST_FUSION_ADAPT_STRUCT(
    ::esd::header::doctype,
    (::std::string, name)
    (::std::string, value)
)
BOOST_FUSION_ADAPT_STRUCT(
    ::esd::header::doctype_version,
    (::std::string, name)
    (::boost::uint64_t, value)
)
BOOST_FUSION_ADAPT_STRUCT(
    ::esd::header::doctype_read_version,
    (::std::string, name)
    (::boost::uint64_t, value)
)

namespace esd {
  namespace range {
    struct fixed_value;
    struct closed_range;
    struct right_opened_range;
    struct left_opened_range;

    template< typename range_type_t >
    struct range;

    template< >
    struct range< fixed_value > {
        template< typename value_type_t >
        struct value_range {
            value_type_t value;
        };
    };
    template< >
    struct range< closed_range > {
        template< typename value_type_t >
        struct value_range {
            value_type_t from;
            value_type_t to;
        };
    };
    template< >
    struct range< right_opened_range > {
        template< typename value_type_t >
        struct value_range {
            value_type_t from;
        };
    };
    template< >
    struct range< left_opened_range > {
        template< typename value_type_t >
        struct value_range {
            value_type_t to;
        };
    };

    typedef range< fixed_value >::value_range< ::boost::int64_t > int_range_fixed_t;
    typedef range< closed_range >::value_range< ::boost::int64_t > int_range_closed_t;
    typedef range< right_opened_range >::value_range< ::boost::int64_t > int_range_right_opened_t;
    typedef range< left_opened_range >::value_range< ::boost::int64_t > int_range_left_opened_t;

    typedef ::boost::variant< int_range_fixed_t, int_range_closed_t, int_range_right_opened_t, int_range_left_opened_t >
        int_range_t;

    typedef range< fixed_value >::value_range< ::boost::uint64_t > uint_range_fixed_t;
    typedef range< closed_range >::value_range< ::boost::uint64_t > uint_range_closed_t;

    typedef ::boost::variant< uint_range_fixed_t, uint_range_closed_t > uint_range_t;

  } // namespace range
} // namespace esd

namespace esd {
  struct element {
  };

  struct type {
      ::std::string name;
      ::std::string type;
  };

  struct header_block {
      ::std::string name;
      ::std::vector< ::esd::header_t > headers;
  };
  struct element_block {
      ::std::string name;
      ::std::vector< ::esd::element > elements;
  };
  struct type_block {
      ::std::string name;
      ::std::vector< ::esd::type > types;
  };

  typedef ::boost::variant< header_block, element_block, type_block > block_t;

  typedef ::std::vector< ::esd::block_t > document;

} // namespace ebml

BOOST_FUSION_ADAPT_STRUCT(
    ::esd::header_block,
    (::std::string, name)
    (::std::vector< ::esd::header_t >, headers)
)
BOOST_FUSION_ADAPT_STRUCT(
    ::esd::element_block,
    (::std::string, name)
    (::std::vector< ::esd::element >, elements)
)
BOOST_FUSION_ADAPT_STRUCT(
    ::esd::type_block,
    (::std::string, name)
    (::std::vector< ::esd::type >, types)
)

template< typename Iterator >
struct esd_grammar: qi::grammar< Iterator, ::esd::document(), qi::ascii::space_type > {
    esd_grammar() :
      esd_grammar::base_type(document) {
      using namespace qi::labels;
      using ::boost::phoenix::at_c;
      using ::boost::phoenix::push_back;

      c_unsigned_integer %= qi::ulong_long;
      c_string %= qi::lexeme[qi::ascii::char_('"') >> +(qi::ascii::char_(0x20, 0x7E) - '"') >> qi::ascii::char_('"')];

      h_ebml_version %= qi::ascii::string("EBMLVersion") >> ":=" >> c_unsigned_integer >> ";";
      h_ebml_read_version %= qi::ascii::string("EBMLReadVersion") >> ":=" >> c_unsigned_integer >> ";";
      h_ebml_max_id_length %= qi::ascii::string("EBMLMaxIDLength") >> ":=" >> c_unsigned_integer >> ";";
      h_ebml_max_size_length %= qi::ascii::string("EBMLMaxSizeLength") >> ":=" >> c_unsigned_integer >> ";";
      h_doctype %= qi::ascii::string("DocType") >> ":=" >> c_string >> ";";
      h_doctype_version %= qi::ascii::string("DocTypeVersion") >> ":=" >> c_unsigned_integer >> ";";
      h_doctype_read_version %= qi::ascii::string("DocTypeReadVersion") >> ":=" >> c_unsigned_integer >> ";";

      header = (h_ebml_version | h_ebml_read_version | h_ebml_max_id_length | h_ebml_max_size_length | h_doctype
          | h_doctype_version | h_doctype_read_version);

      header_block %= qi::lit("declare") >> qi::ascii::string("header") >> "{" >> *header >> "}";

      type_block %= qi::lit("define") >> qi::ascii::string("types") >> "{" >> *type >> "}";

      element_block %= qi::lit("define") >> qi::ascii::string("elements") >> "{" >> *element >> "}";

      block %= (header_block | type_block | element_block);
      document %= *block;
    }

    qi::rule< Iterator, ::boost::uint64_t(), qi::ascii::space_type > c_unsigned_integer;
    qi::rule< Iterator, ::std::string(), qi::ascii::space_type > c_string;

    qi::rule< Iterator, ::esd::header::ebml_version(), qi::ascii::space_type > h_ebml_version;
    qi::rule< Iterator, ::esd::header::ebml_read_version(), qi::ascii::space_type > h_ebml_read_version;
    qi::rule< Iterator, ::esd::header::ebml_max_id_length(), qi::ascii::space_type > h_ebml_max_id_length;
    qi::rule< Iterator, ::esd::header::ebml_max_size_length(), qi::ascii::space_type > h_ebml_max_size_length;
    qi::rule< Iterator, ::esd::header::doctype(), qi::ascii::space_type > h_doctype;
    qi::rule< Iterator, ::esd::header::doctype_version(), qi::ascii::space_type > h_doctype_version;
    qi::rule< Iterator, ::esd::header::doctype_read_version(), qi::ascii::space_type > h_doctype_read_version;

    qi::rule< Iterator, ::esd::header_t(), qi::ascii::space_type > header;
    qi::rule< Iterator, ::esd::header_block(), qi::ascii::space_type > header_block;

    qi::rule< Iterator, ::esd::element(), qi::ascii::space_type > element;
    qi::rule< Iterator, ::esd::element_block(), qi::ascii::space_type > element_block;

    qi::rule< Iterator, ::esd::type(), qi::ascii::space_type > type;
    qi::rule< Iterator, ::esd::type_block(), qi::ascii::space_type > type_block;

    qi::rule< Iterator, ::esd::block_t(), qi::ascii::space_type > block;
    qi::rule< Iterator, ::esd::document(), qi::ascii::space_type > document;
};

int main(int argc, char **argv) {
  namespace qi = ::boost::spirit::qi;

  char const* filename;
  if (argc > 1) {
    filename = argv[1];
  } else {
    ::std::cerr << "Error: No input file provided." << ::std::endl;
    return 1;
  }

  ::std::ifstream in(filename, ::std::ios_base::in);

  if (!in) {
    ::std::cerr << "Error: Could not open input file: " << filename << ::std::endl;
    return 1;
  }

  ::std::string storage; // We will read the contents here.
  in.unsetf(::std::ios::skipws); // No white space skipping!
  ::std::copy(::std::istream_iterator< char >(in), ::std::istream_iterator< char >(), ::std::back_inserter(storage));

  esd_grammar< ::std::string::const_iterator > grammar;
  esd::document ast;

  std::string::const_iterator iter = storage.begin();
  std::string::const_iterator end = storage.end();

  bool r = qi::phrase_parse(iter, end, grammar, qi::ascii::space, ast);
  if (r && iter == end) {
    ::std::cout << "-------------------------\n";
    ::std::cout << "Parsing succeeded\n";
    ::std::cout << "-------------------------\n";
    return 0;
  } else {
    ::std::string::const_iterator some = iter + 30;
    ::std::string context(iter, (some > end) ? end : some);
    ::std::cout << "-------------------------\n";
    ::std::cout << "Parsing failed\n";
    ::std::cout << "stopped at: \": " << context << "...\"\n";
    ::std::cout << "-------------------------\n";
    return 1;
  }

}
