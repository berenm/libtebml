#!/usr/bin/python
# -*- coding: utf-8 -*-

from pyparsing import *

# ======================================================================================================
# Literals
# ======================================================================================================
l_cm   = Suppress(Literal(","))
l_sc   = Suppress(Literal(";"))
l_co   = Suppress(Literal(":"))
l_qt   = Suppress(Literal('"'))
l_as   = Suppress(Literal(":="))
l_osb  = Suppress(Literal("["))
l_csb  = Suppress(Literal("]"))
l_ocb  = Suppress(Literal("{"))
l_ccb  = Suppress(Literal("}"))
l_hex  = Literal("0x")

l_to   = Literal("..")
l_lt   = Literal("<")
l_le   = Literal("<=")
l_gt   = Literal(">")
l_ge   = Literal(">=")
l_lorg = Or([l_lt, l_le, l_gt, l_ge])
l_lowe = Or([l_lt, l_le])

l_m  = Literal("-")
l_p  = Literal("+")
l_mp = Or([l_m, l_p])

# ======================================================================================================
# Structures
# ======================================================================================================
v_name = Combine(Word(alphas + "_", alphanums + "_", min=2))
v_id   = Combine(OneOrMore(Word(hexnums, exact=2)))
v_date = Combine(Word(nums, exact=4) + Word(nums, exact=2) + Word(nums, exact=2) + Optional("T" + Word(nums, exact=2) + l_co + Word(nums, exact=2) + l_co + Word(nums, exact=2) + Optional("." + Optional(Word(nums)))))

# ======================================================================================================
# Default property values
# ======================================================================================================
v_default_int    = Combine(Optional(l_m) + Word(nums))
v_default_uint   = Combine(Word(nums))
v_default_float  = Combine(v_default_int + "." + Word(nums) + Optional("e" + Optional(l_mp) + Word(nums)))
v_default_date   = Or([v_default_int, v_date])
v_string_hex     = Combine(l_hex + OneOrMore(Word(hexnums, exact=2)))
v_string_lit     = Combine(l_qt + Optional(Word(srange(r'[ !\0x23-\0x7E]'))) + l_qt)
v_default_string = Or([v_string_hex, v_string_lit])
v_default_binary = v_default_string

# ======================================================================================================
# Parent property values
# ======================================================================================================
v_parent = Forward()
v_parent << Group(Or([v_name, v_name + l_cm + v_parent]))

# ======================================================================================================
# Level property values
# ======================================================================================================
v_level = Group(OneOrMore(Word(nums)) + Optional(l_to + ZeroOrMore(Word(nums))))

# ======================================================================================================
# Cardinality property values
# ======================================================================================================
l_zone = Literal("?")
l_zany = Literal("*")
l_one  = Literal("1")
l_any  = Literal("+")
v_card = Or([l_zone, l_zany, l_one, l_any])

# ======================================================================================================
# Ordered property values
# ======================================================================================================
l_yes  = Literal("yes")
l_no   = Literal("no")
l_one  = Literal("1")
l_zero = Literal("0")
v_yes  = Or([l_yes, l_one])
v_no   = Or([l_no, l_zero])
v_ordered = Or([v_yes, v_no])

# ======================================================================================================
# Range property values
# ======================================================================================================
v_range_int_item    = Group(Or([v_default_int,
                          v_default_int + l_to,
                          l_to + v_default_int,
                          v_default_int + l_to + v_default_int]))
v_range_int    = Forward()
v_range_int    << Group(Or([v_range_int_item, v_range_int_item + l_cm + v_range_int]))

v_range_uint_item   = Group(v_default_uint + Optional(l_to + v_default_uint))
v_range_uint   = Forward()
v_range_uint   << Group(Or([v_range_uint_item, v_range_uint_item + l_cm + v_range_uint]))

v_range_float_item  = Group(Or([l_lorg + v_default_float,
                          v_default_float + l_lowe + l_to + l_lowe + v_default_float]))
v_range_float  = Forward()
v_range_float  << Group(Or([v_range_float_item, v_range_float_item + l_cm + v_range_float]))

v_range_date_item   = Group(Or([v_default_date + l_to,
                          l_to + v_default_date,
                          v_default_date + l_to + v_default_date]))
v_range_date   = Forward()
v_range_date   << Group(Or([v_range_date_item, v_range_date_item + l_cm + v_range_date]))

v_range_string_item = Group(v_range_uint_item)
v_range_string = Forward()
v_range_string << Group(Or([v_range_string_item, v_range_string_item + l_cm + v_range_string]))

v_range_binary_item = Group(v_range_uint_item)
v_range_binary = Forward()
v_range_binary << Group(Or([v_range_binary_item, v_range_binary_item + l_cm + v_range_binary]))

# ======================================================================================================
# Size property values
# ======================================================================================================
v_size = Forward()
v_size << Or([v_range_uint, v_range_uint + l_cm + v_size])

# ======================================================================================================
# Keywords
# ======================================================================================================
k_declare  = Suppress(Keyword("declare"))
k_define   = Suppress(Keyword("define"))
k_header   = Keyword("header")
k_types    = Keyword("types")
k_elements = Keyword("elements")
k_children = Literal("%children;")

k_container = Keyword("container")
k_int       = Keyword("int")
k_uint      = Keyword("uint")
k_float     = Keyword("float")
k_string    = Keyword("string")
k_date      = Keyword("date")
k_binary    = Keyword("binary")

# ======================================================================================================
# Properties
# ======================================================================================================
k_card    = Keyword("card")
k_default = Keyword("def")
k_level   = Keyword("level")
k_ordered = Keyword("ordered")
k_parent  = Keyword("parent")
k_range   = Keyword("range")
k_size    = Keyword("size")

p_card    = k_card    + l_co + v_card    + l_sc
p_level   = k_level   + l_co + v_level   + l_sc
p_ordered = k_ordered + l_co + v_ordered + l_sc
p_parent  = k_parent  + l_co + v_parent  + l_sc
p_size    = k_size    + l_co + v_size    + l_sc

p_default_int    = k_default + l_co + v_default_int    + l_sc
p_default_uint   = k_default + l_co + v_default_uint   + l_sc
p_default_float  = k_default + l_co + v_default_float  + l_sc
p_default_date   = k_default + l_co + v_default_date   + l_sc
p_default_string = k_default + l_co + v_default_string + l_sc
p_default_binary = k_default + l_co + v_default_binary + l_sc

p_range_int    = k_range + l_co + v_range_int    + l_sc
p_range_uint   = k_range + l_co + v_range_uint   + l_sc
p_range_float  = k_range + l_co + v_range_float  + l_sc
p_range_date   = k_range + l_co + v_range_date   + l_sc
p_range_string = k_range + l_co + v_range_string + l_sc
p_range_binary = k_range + l_co + v_range_binary + l_sc

p_property_int    = Or([p_card, p_level, p_ordered, p_parent, p_size, p_default_int,    p_range_int   ])
p_property_uint   = Or([p_card, p_level, p_ordered, p_parent, p_size, p_default_uint,   p_range_uint  ])
p_property_float  = Or([p_card, p_level, p_ordered, p_parent, p_size, p_default_float,  p_range_float ])
p_property_date   = Or([p_card, p_level, p_ordered, p_parent, p_size, p_default_date,   p_range_date  ])
p_property_string = Or([p_card, p_level, p_ordered, p_parent, p_size, p_default_string, p_range_string])
p_property_binary = Or([p_card, p_level, p_ordered, p_parent, p_size, p_default_binary, p_range_binary])
p_property        = Or([p_card, p_level, p_ordered, p_parent, p_size])

properties_int    = l_osb + Group(OneOrMore(p_property_int   )) + l_csb
properties_uint   = l_osb + Group(OneOrMore(p_property_uint  )) + l_csb
properties_float  = l_osb + Group(OneOrMore(p_property_float )) + l_csb
properties_date   = l_osb + Group(OneOrMore(p_property_date  )) + l_csb
properties_string = l_osb + Group(OneOrMore(p_property_string)) + l_csb
properties_binary = l_osb + Group(OneOrMore(p_property_binary)) + l_csb
properties        = l_osb + Group(OneOrMore(p_property       )) + l_csb

# ======================================================================================================
# Types
# ======================================================================================================
t_contnr = Or([k_container, v_name])
t_int    = Or([k_int,       v_name])
t_uint   = Or([k_uint,      v_name])
t_float  = Or([k_float,     v_name])
t_date   = Or([k_date,      v_name])
t_string = Or([k_string,    v_name])
t_binary = Or([k_binary,    v_name])
t_value  = Or([t_int, t_uint, t_float, t_date, t_string, t_binary])
t_any    = Or([t_value, t_contnr])

t_decl_contnr = v_name + l_as + t_contnr + Or([properties        + Optional(l_sc), l_sc])
t_decl_int    = v_name + l_as + t_int    + Or([properties_int    + Optional(l_sc), l_sc])
t_decl_uint   = v_name + l_as + t_uint   + Or([properties_uint   + Optional(l_sc), l_sc])
t_decl_float  = v_name + l_as + t_float  + Or([properties_float  + Optional(l_sc), l_sc])
t_decl_date   = v_name + l_as + t_date   + Or([properties_date   + Optional(l_sc), l_sc])
t_decl_string = v_name + l_as + t_string + Or([properties_string + Optional(l_sc), l_sc])
t_decl_binary = v_name + l_as + t_binary + Or([properties_binary + Optional(l_sc), l_sc])
t_decl_value  = Or([t_decl_int, t_decl_uint, t_decl_float, t_decl_date, t_decl_string, t_decl_binary])
t_decl_any    = Or([t_decl_contnr, t_decl_value])

t_block = k_define + k_types + l_ocb + Dict(ZeroOrMore(Group(t_decl_any))) + l_ccb

# ======================================================================================================
# Elements
# ======================================================================================================
e_any = Forward()
e_contnr = v_name + l_as + v_id + t_contnr + Optional(properties)  + Or([l_ocb + Dict(ZeroOrMore(Group(e_any))) + l_ccb, l_sc])
e_int    = v_name + l_as + v_id + t_int    + Or([properties_int    + Optional(l_sc), l_sc])
e_uint   = v_name + l_as + v_id + t_uint   + Or([properties_uint   + Optional(l_sc), l_sc])
e_float  = v_name + l_as + v_id + t_float  + Or([properties_float  + Optional(l_sc), l_sc])
e_date   = v_name + l_as + v_id + t_date   + Or([properties_date   + Optional(l_sc), l_sc])
e_string = v_name + l_as + v_id + t_string + Or([properties_string + Optional(l_sc), l_sc])
e_binary = v_name + l_as + v_id + t_binary + Or([properties_binary + Optional(l_sc), l_sc])
e_value  = Or([e_int, e_uint, e_float, e_date, e_string, e_binary])
e_any   << Or([e_value, e_contnr, k_children])

e_block = k_define + k_elements + l_ocb + Dict(ZeroOrMore(Group(e_any))) + l_ccb

# ======================================================================================================
# Header
# ======================================================================================================
k_version              = Keyword("EBMLVersion")
k_read_version         = Keyword("EBMLReadVersion")
k_max_id_length        = Keyword("EBMLMaxIDLength")
k_max_size_length      = Keyword("EBMLMaxSizeLength")
k_doctype              = Keyword("DocType")
k_doctype_version      = Keyword("DocTypeVersion")
k_doctype_read_version = Keyword("DocTypeReadVersion")

h_version              = k_version              + l_as + v_default_uint   + l_sc
h_read_version         = k_read_version         + l_as + v_default_uint   + l_sc
h_max_id_length        = k_max_id_length        + l_as + v_default_uint   + l_sc
h_max_size_length      = k_max_size_length      + l_as + v_default_uint   + l_sc
h_doctype              = k_doctype              + l_as + v_default_string + l_sc
h_doctype_version      = k_doctype_version      + l_as + v_default_uint   + l_sc
h_doctype_read_version = k_doctype_read_version + l_as + v_default_uint   + l_sc
h_statement = Or([h_version, h_read_version, h_max_id_length, h_max_size_length, h_doctype, h_doctype_version, h_doctype_read_version])

h_block = k_declare + k_header + l_ocb + Dict(ZeroOrMore(Group(h_statement))) + l_ccb

# ======================================================================================================
# DTD
# ======================================================================================================
dtd = Dict(ZeroOrMore(Group(Or([h_block, t_block, e_block]))))
dtd.ignore(cStyleComment)
dtd.ignore(cppStyleComment)

e_block.setDebug()
tokens = e_block.parseString("""
   define elements {
     Segment := 18538067 container [ card:*; ] {
 
       // Cluster
       Cluster := 1f43b675 container [ card:*; ] {
         Timecode := e7 uint;
         Position := a7 uint;
         PrevSize := ab uint;
         BlockGroup := a0 container [ card:*; ] {
           Block := a1 binary;
           BlockVirtual := a2 binary;
           BlockAdditions := 75a1 container {
             BlockMore := a6 container [ card:*; ] {
               BlockAddID := ee uint [ range:1..; ]
               BlockAdditional := a5 binary;
             }
           }
           BlockDuration := 9b uint [ def:TrackDuration; ];
           ReferencePriority := fa uint;
           ReferenceBlock := fb int [ card:*; ]
           ReferenceVirtual := fd int;
           CodecState := a4 binary;
           Slices := 8e container [ card:*; ] {
             TimeSlice := e8 container [ card:*; ] {
               LaceNumber := cc uint [ def:0; ]
               FrameNumber := cd uint [ def:0; ]
               BlockAdditionID := cb uint [ def:0; ]
               Delay := ce uint [ def:0; ]
               Duration := cf uint [ def:TrackDuration; ];
             }
           }
         }
       }
     }
   }
""")

#tokens = dtd.parseString(
"""declare header {
     DocType := "matroska";
     EBMLVersion := 1;
   }
   define types {
     bool := uint [ range:0..1; ]
     ascii := string [ range:32..126; ]
   }
   define elements {
     Segment := 18538067 container [ card:*; ] {
 
       // Meta Seek Information
       SeekHead := 114d9b74 container [ card:*; ] {
         Seek := 4dbb container [ card:*; ] {
           SeekID := 53ab binary;
           SeekPosition := 53ac uint;
         }
       }
 
       // Segment Information
       Info := 1549a966 container [ card:*; ] {
         SegmentUID := 73a4 binary;
         SegmentFilename := 7384 string;
         PrevUID := 3cb923 binary;
         PrevFilename := 3c83ab string;
         NextUID := 3eb923 binary;
         NextFilename := 3e83bb string;
         TimecodeScale := 2ad7b1 uint [ def:1000000; ]
         Duration := 4489 float [ range:>0.0; ]
         DateUTC := 4461 date;
         Title := 7ba9 string;
         MuxingApp := 4d80 string;
         WritingApp := 5741 string;
       }
 
       // Cluster
       Cluster := 1f43b675 container [ card:*; ] {
         Timecode := e7 uint;
         Position := a7 uint;
         PrevSize := ab uint;
         BlockGroup := a0 container [ card:*; ] {
           Block := a1 binary;
           BlockVirtual := a2 binary;
           BlockAdditions := 75a1 container {
             BlockMore := a6 container [ card:*; ] {
               BlockAddID := ee uint [ range:1..; ]
               BlockAdditional := a5 binary;
             }
           }
           BlockDuration := 9b uint [ def:TrackDuration; ];
           ReferencePriority := fa uint;
           ReferenceBlock := fb int [ card:*; ]
           ReferenceVirtual := fd int;
           CodecState := a4 binary;
           Slices := 8e container [ card:*; ] {
             TimeSlice := e8 container [ card:*; ] {
               LaceNumber := cc uint [ def:0; ]
               FrameNumber := cd uint [ def:0; ]
               BlockAdditionID := cb uint [ def:0; ]
               Delay := ce uint [ def:0; ]
               Duration := cf uint [ def:TrackDuration; ];
             }
           }
         }
       }
 
       // Track
       Tracks := 1654ae6b container [ card:*; ] {
         TrackEntry := ae container [ card:*; ] {
           TrackNumber := d7 uint [ range:1..; ]
           TrackUID := 73c5 uint [ range:1..; ]
           TrackType := 83 uint [ range:1..254; ]
           FlagEnabled := b9 uint [ range:0..1; def:1; ]
           FlagDefault := 88 uint [ range:0..1; def:1; ]
           FlagLacing  := 9c uint [ range:0..1; def:1; ]
           MinCache := 6de7 uint [ def:0; ]
           MaxCache := 6df8 uint;
           DefaultDuration := 23e383 uint [ range:1..; ]
           TrackTimecodeScale := 23314f float [ range:>0.0; def:1.0; ]
           Name := 536e string;
           Language := 22b59c string [ def:"eng"; range:32..126; ]
           CodecID := 86 string [ range:32..126; ];
           CodecPrivate := 63a2 binary;
           CodecName := 258688 string;
           CodecSettings := 3a9697 string;
           CodecInfoURL := 3b4040 string [ card:*; range:32..126; ]
           CodecDownloadURL := 26b240 string [ card:*; range:32..126; ]
           CodecDecodeAll := aa uint [ range:0..1; def:1; ]
           TrackOverlay := 6fab uint;
 
           // Video
           Video := e0 container {
             FlagInterlaced := 9a uint [ range:0..1; def:0; ]
             StereoMode := 53b8 uint [ range:0..3; def:0; ]
             PixelWidth := b0 uint [ range:1..; ]
             PixelHeight := ba uint [ range:1..; ]
             DisplayWidth := 54b0 uint [ def:PixelWidth; ]
             DisplayHeight := 54ba uint [ def:PixelHeight; ]
             DisplayUnit := 54b2 uint [ def:0; ]
             AspectRatioType := 54b3 uint [ def:0; ]
             ColourSpace := 2eb524 binary;
             GammaValue := 2fb523 float [ range:>0.0; ]
           }
 
           // Audio
           Audio := e1 container {
             SamplingFrequency := b5 float [ range:>0.0; def:8000.0; ]
             OutputSamplingFrequency := 78b5 float [ range:>0.0;
                                                     def:8000.0; ]
             Channels := 94 uint [ range:1..; def:1; ]
             ChannelPositions := 7d7b binary;
             BitDepth := 6264 uint [ range:1..; ]
           }
 
           // Content Encoding
           ContentEncodings := 6d80 container {
             ContentEncoding := 6240 container [ card:*; ] {
               ContentEncodingOrder := 5031 uint [ def:0; ]
               ContentEncodingScope := 5032 uint [ range:1..; def:1; ]
               ContentEncodingType := 5033 uint;
               ContentCompression := 5034 container {
                 ContentCompAlgo := 4254 uint [ def:0; ]
                 ContentCompSettings := 4255 binary;
               }
               ContentEncryption := 5035 container {
                 ContentEncAlgo := 47e1 uint [ def:0; ]
                 ContentEncKeyID := 47e2 binary;
                 ContentSignature := 47e3 binary;
                 ContentSigKeyID := 47e4 binary;
                 ContentSigAlgo := 47e5 uint;
                 ContentSigHashAlgo := 47e6 uint;
               }
             }
           }
         }
       }
 
       // Cueing Data
       Cues := 1c53bb6b container {
         CuePoint := bb container [ card:*; ] {
           CueTime := b3 uint;
           CueTrackPositions := b7 container [ card:*; ] {
             CueTrack := f7 uint [ range:1..; ]
             CueClusterPosition := f1 uint;
             CueBlockNumber := 5378 uint [ range:1..; def:1; ]
             CueCodecState := ea uint [ def:0; ]
             CueReference := db container [ card:*; ] {
               CueRefTime := 96 uint;
               CueRefCluster := 97 uint;
               CueRefNumber := 535f uint [ range:1..; def:1; ]
               CueRefCodecState := eb uint [ def:0; ]
             }
           }
         }
       }
 
       // Attachment
       Attachments := 1941a469 container {
         AttachedFile := 61a7 container [ card:*; ] {
           FileDescription := 467e string;
           FileName := 466e string;
           FileMimeType := 4660 string [ range:32..126; ]
           FileData := 465c binary;
           FileUID := 46ae uint;
         }
       }
 
       // Chapters
       Chapters := 1043a770 container {
         EditionEntry := 45b9 container [ card:*; ] {
           ChapterAtom := b6 container [ card:*; ] {
             ChapterUID := 73c4 uint [ range:1..; ]
             ChapterTimeStart := 91 uint;
             ChapterTimeEnd := 92 uint;
             ChapterFlagHidden := 98 uint [ range:0..1; def:0; ]
             ChapterFlagEnabled := 4598 uint [ range:0..1; def:0; ]
             ChapterTrack := 8f container {
               ChapterTrackNumber := 89 uint [ card:*; range:0..1; ]
               ChapterDisplay := 80 container [ card:*; ] {
                 ChapString := 85 string;
                 ChapLanguage := 437c string [ card:*; def:"eng";
                                               range:32..126; ]
                 ChapCountry := 437e string [ card:*; range:32..126; ]
               }
             }
           }
         }
       }
 
       // Tagging
       Tags := 1254c367 container [ card:*; ] {
         Tag := 7373 container [ card:*; ] {
           Targets := 63c0 container {
             TrackUID := 63c5 uint [ card:*; def:0; ]
             ChapterUID := 63c4 uint [ card:*; def:0; ]
             AttachmentUID := 63c6 uint [ card:*; def:0; ]
           }
           SimpleTag := 67c8 container [ card:*; ] {
             TagName := 45a3 string;
             TagString := 4487 string;
             TagBinary := 4485 binary;
           }
         }
       }
     }
   }
"""
#)

def print_toks(v, k="", p=""):
  if v.__class__.__name__ == str.__name__:
    print p, k, v
  else:
    if k != "":
      print p, k
    if len(v.items()) > 0:
      for (kk, vv) in v.items():
        print_toks(vv, kk, p + "  ")
    else:
      for vv in v:
        print_toks(vv, "", p + "  ")

print_toks(tokens)
